﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WPIntegration;

namespace ExecuteWorkplaceIntegration
{
    class Program
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Program));
        static void Main(string[] args)
        {
            // Configure the logger
            log4net.Config.XmlConfigurator.Configure();

            UserMethods userMethods = new UserMethods(ConfigurationManager.AppSettings["BearerToken"], ConfigurationManager.AppSettings["ProfilePhotoURL"], ConfigurationManager.AppSettings["ProfilePictureIDField"], ConfigurationManager.AppSettings["ProfilePictureFileExtention"]);
            SyncMethods syncMethods = new SyncMethods(ConfigurationManager.AppSettings["BearerToken"]);

            string groupId = ConfigurationManager.AppSettings["SyncGroupId"];
            string domainName = ConfigurationManager.AppSettings["DomainName"];
            string domainUrl = ConfigurationManager.AppSettings["DomainUrl"];

            if (args.Length > 0 && args[0].ToUpper().CompareTo("SYNC") == 0)
            {
                Console.WriteLine("Workplace Integration do a normal sync");
                
                Task.Run(async () =>
                {
                    await syncMethods.SyncGroups(groupId, domainName, domainUrl);
                }).GetAwaiter().GetResult();
            }
            else if (args.Length > 0 && args[0].ToUpper().CompareTo("PHOTOS") == 0)
            {
                Console.WriteLine("Workplace Integration do a photo upload.");
                
                Task.Run(async () =>
                {
                    string result = await userMethods.UpdateAllProfilePictures();
                    Console.WriteLine(result);
                }).GetAwaiter().GetResult();
                
            }
            else
            {
                while (true)
                {
                    Console.WriteLine();
                    Console.WriteLine("Workplace Integration");
                    Console.WriteLine("For a normal use call with the argument SYNC");
                    Console.WriteLine("....");
                    Console.WriteLine("Press A for Active Directory read");
                    Console.WriteLine("Press U to update profile picture of a user");
                    Console.WriteLine("Press Y to update ALL profile pictures");
                    Console.WriteLine("Press G to sync groups");
                    Console.WriteLine("Press T to test sync");
                    Console.WriteLine("Press P to test profile picture sync");
                    Console.WriteLine("");
                    Console.WriteLine("Press Q to quit");
                    Console.WriteLine();

                    string readKey = Console.ReadKey(true).KeyChar.ToString().ToLower();

                    switch (readKey) {
                        case "q":
                            return;

                        case "a":
                            doActiveDirectoryRead();
                            break;
                        case "u":
                            Console.WriteLine("Enter the UserId you want to update:");
                            string userid = Console.ReadLine();
                            Task.Run(async () =>
                            {
                                bool result = await userMethods.UpdateProfilePicture(userid);
                                Console.WriteLine(String.Concat("Was the profile updated? ", result));
                            }).GetAwaiter().GetResult();
                            break;
                        case "y":
                            Task.Run(async () =>
                            {
                                string result = await userMethods.UpdateAllProfilePictures();
                                Console.WriteLine(result);
                            }).GetAwaiter().GetResult();
                            break;
                        case "g":
                            Task.Run(async () =>
                            {
                                await syncMethods.SyncGroups(groupId, domainName, domainUrl);
                            }).GetAwaiter().GetResult();
                            break;
                        case "t":
                            Task.Run(async () =>
                            {
                                await syncMethods.SyncGroups(groupId, domainName, domainUrl, true);
                            }).GetAwaiter().GetResult();
                            break;
                        case "p":
                            Task.Run(async () =>
                            {
                                string result = await userMethods.UpdateAllProfilePictures(true);
                                Console.WriteLine(result);
                            }).GetAwaiter().GetResult();
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private static void doActiveDirectoryRead()
        {
            Console.Clear();
            Console.WriteLine("Doing Active Directory read");
            Console.ReadKey();

            //blhe blhe
        }
    }
}
