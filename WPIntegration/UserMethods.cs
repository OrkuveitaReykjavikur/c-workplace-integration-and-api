﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkplaceAPI;
using WorkplaceAPI.Models;

namespace ExecuteWorkplaceIntegration
{
    public class UserMethods
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(UserMethods));
        private AccountApi Api;
        private GraphApi Gapi;
        private string ProfilePhotoURL;
        private string ProfilePictureIDField = "pager";
        private string ProfilePictureFileExtension = "jpg";
        private string ProfilePictureFileExtensionType = "image/jpeg";

        public UserMethods(string bearerToken, string profilePhotoURL, string profilePictureIDField, string profilePictureFileExtension)
        {
            this.Api = new AccountApi(bearerToken);
            this.Gapi = new GraphApi(bearerToken);
            this.ProfilePhotoURL = profilePhotoURL;
            this.ProfilePictureIDField = profilePictureIDField;
            this.ProfilePictureFileExtension = profilePictureFileExtension;
            if (ProfilePictureFileExtension == "png")
            {
                ProfilePictureFileExtensionType = "image/png";
            }
        }

        /// <summary>
        /// Only updates the profile photo if it doesn't previously exist
        /// </summary>
        /// <param name="userId"></param>
        /// <returns><c>True</c> if the profile picture was successfully updated, otherwise if it wasn't updated or the update failed <c>False</c></returns>
        public async Task<bool> UpdateProfilePicture(string userId)
        {
            User user = await Api.Users.GetUser(userId);
            return await UpdateProfilePicture(user);
        }

        /// <summary>
        /// Update the profile picture of the provided user.
        /// </summary>
        /// <remarks>
        /// We use a identifier that's stored in a field under the phonenumber property to find the
        /// correct picture. The default field is "pager"
        /// For some reason Facebook changes this number to a US phone number
        /// if it's starts with numbers from an US area code.
        /// </remarks>
        private async Task<bool> UpdateProfilePicture(User user, bool onlyLog = false)
        {
            if (user != null)
            {

                var profilePhoto = user.photos.FirstOrDefault(a => a.type == "profile");
                var kennitala = user.phoneNumbers.FirstOrDefault(a => a.type == ProfilePictureIDField);

                if (kennitala != null)
                {
                    //"+1-980-291-2959"
                    // Remove the changes Facebook made to the number
                    if (kennitala.value.StartsWith("+"))
                    {
                        string strKt = kennitala.value.Replace("-", String.Empty);
                        if (strKt.Length > 10)
                        {
                            strKt = strKt.Substring(strKt.Length - 10, 10);
                        }
                        kennitala.value = strKt;
                    }

                    // Get the URL to the profile photo for this user
                    string filename = kennitala.value + "." + ProfilePictureFileExtension;
                    string photoUrl = ProfilePhotoURL + "\\" + filename;

                    if (profilePhoto == null && photoUrl.StartsWith("http"))
                    {
                        user.photos.Add(new CommunicationProperties { primary = true, type = "profile", value = photoUrl });
                    }
                    if (profilePhoto == null && !photoUrl.StartsWith("http"))
                    {
                        if (onlyLog)
                        {
                            logger.Info(String.Format("Location of profile picture for the user {0} ({1}) is {2}", user.name.formatted, user.id, photoUrl));
                            return true;
                        }
                        byte[] file = System.IO.File.ReadAllBytes(photoUrl);
                        ProfilePicture profilePicture = new ProfilePicture { Content = file, Filename = filename, Type = ProfilePictureFileExtensionType };
                        return await Gapi.Users.UpdateUserPicture(profilePicture, user.id);

                    }

                    if (profilePhoto != null && !profilePhoto.value.Equals(photoUrl))
                    {
                        profilePhoto.value = photoUrl;
                        // Update the user
                        logger.Info(String.Format("Location of profile picture for the user {0} ({1}) is {2}", user.name.formatted, user.id, photoUrl));
                        if (onlyLog)
                        {
                            return true;
                        }
                        return await Api.Users.UpdateUser(user, user.id) != null;
                    }

                }

                
            }
            return false;
        }

        public async Task<string> UpdateAllProfilePictures(bool onlyLog = false)
        {
            logger.Info(String.Format("Starting Workplace PHOTO upload."));
            StringBuilder result = new StringBuilder();
            StringBuilder resultNoChange = new StringBuilder();
            // We will need to know what Users have an uploaded photo to exclude them from unnecessary uploads or futher profile photo modifications
            // This info was not avalible using the SCIM API and therfore we would need to also utilize the Graph API. 
            // Utilzing only the Graph API (and Member Class) in this logic or just for the Upload function meant to much rewrite
            //Therfore we are mixing the two APIS and Member/User Object. 
            List<Member> members = await Gapi.Users.GetAllCompanyOrganizationMembers();
            List<User> users = await Api.Users.GetAllUsers();
            // Do not update users that already have a photo
            List<Member> membersAlreadyWithProfilePicture = new List<Member>();

            // Create a list of Members that already have a photo
            foreach (Member member in members)
            {
                try
                {
                    if (!member.picture.data.is_silhouette)
                    {
                        membersAlreadyWithProfilePicture.Add(member);
                    }
                }
                catch (Exception e)
                {
                    result.AppendLine(String.Format("Exception: {0}.", e.Message));
                }
            }

            foreach (User user in users)
            {
                try
                {
                    if (!user.active) 
                    {
                        continue;
                    }
                    bool update = true;
                    // Check if the User already has a profile photo and if so not update
                    foreach (Member memberAlreadyWithProfilePicture in membersAlreadyWithProfilePicture)
                    {
                        if (user.id == memberAlreadyWithProfilePicture.id)
                        {
                            update = false;
                            break;
                        }
                    }
                    bool updated = false;
                    if (update)
                    {
                        updated = await UpdateProfilePicture(user, onlyLog);
                    }
                    if (updated)
                    {
                        logger.Info(String.Format("Profile picture was added for the user {0} ({1}).", user.name.formatted, user.id));
                        result.AppendLine(String.Format("Profile picture was added for the user {0} ({1}).", user.name.formatted, user.id));
                    }
                    else
                    {
                        resultNoChange.AppendLine(String.Format("No change for user {0} ({1}).", user.name.formatted, user.id));
                    }
                }
                catch (Exception e)
                {
                    logger.Error(String.Format("Error occurred while updating the profile picture for the user {0} ({1}).", user.name.formatted, user.id), e);
                    result.AppendLine(String.Format("Error occurred while updating the profile picture for the user {0} ({1}).", user.name.formatted, user.id));
                    result.AppendLine(String.Format("Exception: {0}.", e.Message));
                }
            }

            resultNoChange.Append(result);
            logger.Info(String.Format("Ending Workplace PHOTO upload."));
            return resultNoChange.ToString();
        }


    }
}