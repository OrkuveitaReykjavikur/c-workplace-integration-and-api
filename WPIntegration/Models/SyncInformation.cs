﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPIntegration.Models
{
    public class SyncInformation
    {
        public string groupId { get; set; }
        public List<string> usersToAdd { get; set; }
        public List<string> usersToRemove { get; set; }
    }
}