﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ActiveDirectoryAPI.ADClasses;
using WorkplaceAPI;
using WorkplaceAPI.Models;
using WPIntegration.Models;

namespace WPIntegration
{
    public class SyncMethods
    {
        private GraphApi GraphApi;
        private AccountApi AccountApi;
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SyncMethods));

        public SyncMethods(string bearerToken)
        {
            this.GraphApi = new GraphApi(bearerToken);
            this.AccountApi = new AccountApi(bearerToken);
        }

        /// <summary>
        /// Sync ActiveDirectory and Workplace groups.
        /// </summary>
        /// <param name="groupId">The workplace group that contains the configuration</param>
        /// <param name="domainName">The domain name in AD</param>
        /// <param name="domainUrl">The domain URL in AD</param>
        /// <param name="onlyLog">True if you only want to log, otherwise false</param>
        public async Task SyncGroups(string groupId, string domainName, string domainUrl, bool onlyLog = false)
        {
            logger.Info(String.Format("Starting ActiveDirectory to Workplace Group sync."));
            List<SyncInformation> syncInformation = await GetSyncInformation(groupId, domainName, domainUrl);

            foreach (var info in syncInformation)
            {
                foreach (var user in info.usersToRemove)
                {
                    logger.Info(String.Format("Removing the user {0} from the group {1}.", user, info.groupId));
                    if (!onlyLog)
                    {
                        try
                        {
                            await GraphApi.Groups.RemoveMember(info.groupId, user);
                        }
                        catch (Exception e)
                        {
                            logger.Error(String.Format("Error occurred while removing {0} from the group {1}.", user, info.groupId), e);
                        }
                    }
                }

                foreach (var user in info.usersToAdd)
                {
                    logger.Info(String.Format("Adding the user {0} to the group {1}.", user, info.groupId));
                    if (!onlyLog)
                    {
                        try
                        {
                            await GraphApi.Groups.AddMember(info.groupId, user);
                        }
                        catch (Exception e)
                        {
                            logger.Error(String.Format("Error occurred while adding {0} to the group {1}.", user, info.groupId), e);
                        }
                        
                    }
                }
            }
            logger.Info(String.Format("End of ActiveDirectory to Workplace Group sync."));
        }

        /// <summary>
        /// Parse the configuration defined in the provided group.
        /// </summary>
        /// <remarks>
        /// For every group defined in the Workplace configuration group we want to 
        /// create lists of members that have to be added and removed.
        /// However we don't want to remove Admins of Workplace groups, and we don't want to try
        /// to add members to Workplace groups that don't already have a Workplace account.
        /// </remarks>
        /// <returns>A list of users that have to be added or removed from each group defined in the config</returns>
        public async Task<List<SyncInformation>> GetSyncInformation(string groupId, string domainName, string domainUrl)
        {
            List<SyncInformation> result = new List<SyncInformation>();
            string resultString = String.Empty;
            int resultCode = 0;

            // Regular expressions used to find "Workplace = {groupId}" and "AD = {adName}".
            // These should exist in the messages from the config group.
            Regex regexWorkplace = new Regex(@"(?<=workplace\s=\s)\d+$", RegexOptions.IgnoreCase | RegexOptions.Multiline);
            Regex regexAD = new Regex(@"(?<=ad\s=\s).+$", RegexOptions.IgnoreCase | RegexOptions.Multiline);

            try
            {
                List<Feed> posts = await GraphApi.Groups.GetPosts(groupId);
                List<User> workplaceUsers = await AccountApi.Users.GetAllUsers();
                var allWorkplaceEmails = workplaceUsers.Select(a => a.userName.ToLower());

                foreach (Feed post in posts.Where(a => !String.IsNullOrEmpty(a.message)))
                {
                    // We ignore configurations starting with this token
                    if (!post.message.StartsWith("[IGNORE]"))
                    {
                        Match workplaceGroup = regexWorkplace.Match(post.message);
                        Match adGroup = regexAD.Match(post.message);

                        if (workplaceGroup.Success && adGroup.Success)
                        {
                            try
                            {
                                // Get all the users that are currently in the configured facebook group
                                List<Member> members = await GraphApi.Groups.GetAllGroupMembers(workplaceGroup.Value);

                                // We don't want to remove admins from the groups
                                var validMembers = members.Where(a => !String.IsNullOrEmpty(a.email));
                                var groupAdmins = validMembers.Where(a => a.administrator).Select(a => a.email);
                                List<string> facebookEmails = validMembers.Select(a => a.email.ToLower()).ToList();

                                // Get all the users that are in the configured AD group
                                List<ADUser> adUsers = ActiveDirectoryAPI.ADApi.getUsersMemberOfGroup(adGroup.Value, domainName, domainUrl, ref resultCode, ref resultString);
                                List<string> adEmails = adUsers.Where(a => !String.IsNullOrEmpty(a.emailAddress)).Select(a => a.emailAddress.ToLower()).ToList();

                                // If there are any users tagged as extra members then we want to sync them as well
                                if (post.to != null)
                                {
                                    foreach (Profile extraMember in post.to.data)
                                    {
                                        User user = workplaceUsers.FirstOrDefault(a => a.id == extraMember.id);
                                        if (user != null)
                                        {
                                            adEmails.Add(user.userName.ToLower());
                                        }
                                    }
                                }

                                // We don't want to remove group admins
                                List<string> usersToRemove = facebookEmails.Except(groupAdmins).Except(adEmails).ToList();

                                // We don't want to try to add users that aren't members of workplace
                                List<string> usersToAdd = adEmails.Intersect(allWorkplaceEmails).Except(facebookEmails).ToList();

                                result.Add(new SyncInformation { groupId = workplaceGroup.Value, usersToAdd = usersToAdd, usersToRemove = usersToRemove });
                            }
                            catch (Exception e)
                            {
                                logger.Error(String.Format("Exception occurred while creating result lists for the following post: {0}.", post.message), e);
                            }
                        }
                        else
                        {
                            logger.Error(String.Format("Ekki tókst að lesa sync config úr þessum skilaboðum: {0}.", post.message));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(String.Format("Error occurred while parsing the configuration."), e);
            }

            return result;
        }
    }
}