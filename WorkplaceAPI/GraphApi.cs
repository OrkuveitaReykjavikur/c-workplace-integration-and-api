﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI
{
    public class GraphApi : Api
    {
        private const string BaseAddress = "https://graph.facebook.com";

        public GraphApi(string bearerToken) : base(BaseAddress, bearerToken) { }

        private Groups groups = null;
        private Users users = null;
        public Groups Groups
        {
            get
            {
                if (groups == null)
                {
                    groups = new Groups(this);
                }
                return groups;
            }
        }
        public Users Users
        {
            get
            {
                if (users == null)
                {
                    users = new Users(this);
                }
                return users;
            }
        }

    }
}
