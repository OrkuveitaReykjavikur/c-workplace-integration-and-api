﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkplaceAPI.Models;

namespace WorkplaceAPI
{
    public interface IApi
    {
        Task<T> Get<T>(string uri) where T : class;
        Task<T> Post<T>(string uri, T content) where T : class;
        Task<string> Post(string uri, string content);
        Task Post (string uri, ProfilePicture profilePicture);
        Task<T> Put<T>(string uri, T content) where T : class;
        Task Delete(string uri);

    }
}