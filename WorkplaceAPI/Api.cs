﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using WorkplaceAPI.Models;

namespace WorkplaceAPI
{
    /// <summary>
    /// Base implementation of the API calls
    /// </summary>
    public class Api : IApi
    {
        private HttpClient client;

        /// <summary>
        /// Initialize the HTTP client for the API calls.
        /// </summary>
        /// <param name="bearerToken">The bearer token provided by workplace</param>
        public Api(string baseAddress, string bearerToken)
        {
            if (String.IsNullOrEmpty(bearerToken))
            {
                throw new ArgumentException("The bearer token must be provided", "bearerToken");
            }

            client = new HttpClient { BaseAddress = new Uri(baseAddress) };
            
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/javascript"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);
        }

        public async Task<T> Get<T>(string uri) where T : class
        {
            HttpResponseMessage response = await client.GetAsync(uri);
            return await DeserializeContent<T>(response);
        }


        public async Task Post(string uri, ProfilePicture profilePicture)
        {
            MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent();
            StreamContent streamContent = new StreamContent(new System.IO.MemoryStream(profilePicture.Content));
            streamContent.Headers.ContentType = new MediaTypeHeaderValue(profilePicture.Type);
            multipartFormDataContent.Add(streamContent, "image_data", "image" + ".jpg");
            HttpResponseMessage response = await client.PostAsync(uri, multipartFormDataContent);
            response.EnsureSuccessStatusCode();
        }

        // TODO: Do I want to return the object? Or just ensure success status?
        public async Task<T> Post<T>(string uri, T content) where T : class
        {
            HttpResponseMessage response = await client.PostAsJsonAsync(uri, content);
            return await DeserializeContent<T>(response);
        }

        public async Task<string> Post(string uri, string content)
        {
            HttpResponseMessage response = await client.PostAsync(uri, new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded"));
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            return null;
        }


        // TODO: Do I want to return the object? Or just ensure success status?
        public async Task<T> Put<T>(string uri, T content) where T : class
        {
            HttpResponseMessage response = await client.PutAsJsonAsync(uri, content);
            return await DeserializeContent<T>(response);
        }

        public async Task Delete(string uri)
        {
            HttpResponseMessage response = await client.DeleteAsync(uri);
            response.EnsureSuccessStatusCode();
        }

        private async Task<T> DeserializeContent<T> (HttpResponseMessage response) where T : class
        {
            T result = null;
            if (response.IsSuccessStatusCode)
            {
                if (response.Content.Headers.ContentType.MediaType == "text/javascript")
                {
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                }

                result = await response.Content.ReadAsAsync<T>();
            }
            return result;
        }
    }
}