﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using WorkplaceAPI.Models;

namespace WorkplaceAPI
{
    public class Users
    {
        private IApi Api;
        private const int DefaultLimit = 100;

        public Users(IApi api)
        {
            this.Api = api;
        }

        public async Task<User> GetUser(string userId)
        {
            string uri = String.Format("Users/{0}",HttpUtility.UrlEncode(userId));
            return await Api.Get<User>(uri);
        }

        /// <summary>
        /// Gets the users from the company for the provided scim id from the start index.
        /// </summary>
        /// <remarks>
        /// Only <see cref="DefaultLimit"/> users are returned by default so if there are more
        /// users than that in the company then there are more users to be found at START_INDEX + DefaultLimt.
        /// </remarks>
        /// <param name="startIndex">The index we want to get users from</param>
        public async Task<UsersResponse> GetUsers(int startIndex = 1)
        {
            string uri = String.Format("Users?count={0}&startIndex={1}",DefaultLimit, startIndex);
            return await Api.Get<UsersResponse>(uri);
        }

        /// <summary>
        /// Gets all the users for the company
        /// </summary>
        /// <returns>all the users in the company</returns>
        public async Task<List<User>> GetAllUsers()
        {
            List<User> result = new List<User>();
            await GetAllUsers(result);
            return result;
        }

        /// <summary>
        /// Recursively populates the provided list with all the users in the company.
        /// </summary>
        /// <remarks>
        /// The Facebook API doesn't necessarily return all the users in one call.
        /// In order to get all the users for the provided scim id we recursively
        /// call the API as long as we haven't passed the last index or until
        /// an error occurs, i.e. null was returned from the Get.
        /// </remarks>
        /// <param name="result">The list of users that gets recursively populated</param>
        /// <param name="startIndex">The index we want to get users from</param>
        private async Task GetAllUsers(List<User> result, int startIndex = 1)
        {
            UsersResponse response = await GetUsers(startIndex);

            if (response != null)
            {
                result.AddRange(response.users);

                // Update the start index
                startIndex += DefaultLimit;

                if (startIndex <= response.totalResults)
                {
                    await GetAllUsers(result, startIndex);
                }
            }
        }

        public async Task<User> UpdateUser(User user, string userId)
        {
            string uri = String.Format("Users/{0}",HttpUtility.UrlEncode(userId));
            return await Api.Put<User>(uri, user);
        }
        // Only Graph API Supported
        public async Task<Boolean> UpdateUserPicture(ProfilePicture profilePicture, string userId)
        {
            string uri = String.Format("/{0}/profile_pictures", HttpUtility.UrlEncode(userId));
            await Api.Post(uri,profilePicture);
            return true;
        }
        /// <summary>
        /// Only Graph API Supported
        /// </summary>
        /// 

        public async Task<MembersResponse> GetAllCompanyOrganizationMembers(string after = "")
        {
            string fields = "id,email,picture";
            string uri = String.Format("company/organization_members?limit={0}&fields={1}",DefaultLimit, fields);
            if (!String.IsNullOrEmpty(after))
            {
                uri += String.Format("&after={0}", HttpUtility.UrlEncode(after));
            }
            return await Api.Get<MembersResponse>(uri);
        }

        /// <summary>
        /// Only Graph API Supported
        /// </summary>
        public async Task<List<Member>> GetAllCompanyOrganizationMembers()
        {
            List<Member> result = new List<Member>();
            await GetAllCompanyOrganizationMembers(result);
            return result;
        }
        private async Task GetAllCompanyOrganizationMembers(List<Member> result, string after = "")
        {
            MembersResponse members = await GetAllCompanyOrganizationMembers(after);

            if (members != null)
            {
                result.AddRange(members.data);

                if (!String.IsNullOrEmpty(members.paging.next))
                {
                    await GetAllCompanyOrganizationMembers(result, members.paging.cursors.after);
                }
            }
        }

    }
}