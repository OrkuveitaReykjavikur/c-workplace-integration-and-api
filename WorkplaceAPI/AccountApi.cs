﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI
{
    public class AccountApi : Api
    {
        private const string BaseAddress = "https://scim.workplace.com/";
        public AccountApi(string bearerToken) : base(BaseAddress, bearerToken) { }

        private Users users = null;
        public Users Users
        {
            get
            {
                if (users == null)
                {
                    users = new Users(this);
                }
                return users;
            }
        }
    }
}