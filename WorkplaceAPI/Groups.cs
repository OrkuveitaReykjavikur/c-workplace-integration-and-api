﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkplaceAPI.Models;
using System.Web;
using System.Collections.Generic;

namespace WorkplaceAPI
{
    /// <summary>
    /// Used to query the API for Group related content
    /// </summary>
    /// <remarks>
    /// The Graph API by default uses cursor-based pagination.
    /// That means that possibly not all the records are returned at the same time.
    /// 
    /// If the total number of records exceed the DefaultLimit then we need to 
    /// recursively call the API in order to get all the recrods.
    /// To do that we use the 'after cursor' which points to the end 
    /// of the page of data that has been returned, this is provided by the 
    /// paging information from the API.
    /// 
    /// 
    /// E.g. Get all the groups for a provided community:
    /// 
    /// If the total number of groups in the community exceeds the DefaultLimt 
    /// then we recursively call the API as long as the results 
    /// are paged, i.e.'Next' in the paging information isn't null 
    /// and 'Get' didn't return null.
    /// When 'Next' is null we know that we got all the groups in the community.
    /// </remarks>
    public class Groups
    {
        private IApi Api;
        private const int DefaultLimit = 100;

        internal Groups(GraphApi api)
        {
            this.Api = api;
        }

        #region Get Groups
        public async Task<Group> GetGroup(string groupId)
        {
            string uri = HttpUtility.UrlEncode(groupId);
            return await Api.Get<Group>(uri);
        }

        /// <summary>
        /// Gets the groups for the provided community.
        /// </summary>
        /// <remarks>
        /// To return all groups <see cref="GetAllCommunityGroups(string)"/>.
        /// 
        /// Only <see cref="DefaultLimit"/> groups are returned by default since
        /// the API uses cursor-based pagination.
        /// </remarks>
        /// <param name="communityId">The id of the community provided by Facebook</param>
        /// <param name="after">The cursor that points to the end of the page of data that has been returned, provided by the paging information from the API</param>
        public async Task<CommunityGroupsResponse> GetCommunityGroups(string communityId, string after = "")
        {
            string uri = String.Format("{0}/groups?limit={1}", HttpUtility.UrlEncode(communityId), DefaultLimit);
            if (!String.IsNullOrEmpty(after))
            {
                uri += String.Format("&after={0}", HttpUtility.UrlEncode(after));
            }
            return await Api.Get<CommunityGroupsResponse>(uri);
        }

        /// <summary>
        /// Gets all the community groups for the provided community id.
        /// </summary>
        public async Task<List<Group>> GetAllCommunityGroups(string communityId)
        {
            List<Group> result = new List<Group>();
            await GetAllCommunityGroups(communityId, result);
            return result;
        }

        /// <summary>
        /// Recursively populates the provided list with all the groups in the community.
        /// </summary>
        private async Task GetAllCommunityGroups(string communityId, List<Group> result, string after = "")
        {
            CommunityGroupsResponse groups = await GetCommunityGroups(communityId, after);

            if (groups != null)
            {
                result.AddRange(groups.data);

                if (!String.IsNullOrEmpty(groups.paging.next))
                {
                    await GetAllCommunityGroups(communityId, result, groups.paging.cursors.after);
                }
            }
        }
        #endregion

        #region Group Members
        /// <summary>
        /// Gets the members of the provided group.
        /// </summary>
        /// <remarks>
        /// To return all members <see cref="GetAllGroupMembers(string)"/>.
        /// 
        /// Only <see cref="DefaultLimit"/> members are returned by default since
        /// the API uses cursor-based pagination.
        /// </remarks>
        /// <param name="groupId">The id of the group we want to get the members from</param>
        /// <param name="after">The cursor that points to the end of the page of data that has been returned, provided by the paging information from the API</param>
        public async Task<MembersResponse> GetGroupMembers(string groupId, string after = "")
        {
            string fields = "id,email,picture,link,locale,name,administrator,first_name,last_name,name_format,employee_number";
            string uri = String.Format("{0}/members?limit={1}&fields={2}", HttpUtility.UrlEncode(groupId), DefaultLimit, fields);
            if (!String.IsNullOrEmpty(after))
            {
                uri += String.Format("&after={0}", HttpUtility.UrlEncode(after));
            }
            return await Api.Get<MembersResponse>(uri);
        }

        /// <summary>
        /// Gets all the members for the provided group.
        /// </summary>
        public async Task<List<Member>> GetAllGroupMembers(string groupId)
        {
            List<Member> result = new List<Member>();
            await GetAllGroupMembers(groupId, result);
            return result;
        }

        /// <summary>
        /// Recursively populate the provided list with all the members in the group.
        /// </summary>
        private async Task GetAllGroupMembers(string groupId, List<Member> result, string after = "")
        {
            MembersResponse members = await GetGroupMembers(groupId, after);
            
            if (members != null)
            {
                result.AddRange(members.data);

                if (!String.IsNullOrEmpty(members.paging.next))
                {
                    await GetAllGroupMembers(groupId, result, members.paging.cursors.after);
                }
            }
        }
        
        /// <summary>
        /// Adds a member to the provided group.
        /// </summary>
        /// <param name="groupId">The group that we want to add the user to</param>
        /// <param name="email">The email of the user we want to add</param>
        public async Task AddMember(string groupId, string email)
        {
            string uri = String.Format("{0}/members", HttpUtility.UrlEncode(groupId));
            string content = String.Format("email={0}", HttpUtility.UrlEncode(email));

            await Api.Post(uri, content);
        }

        /// <summary>
        /// Removes a member from the provided group.
        /// </summary>
        /// <param name="groupId">The group that we want to remove the user from</param>
        /// <param name="email">The email of the user we want to remove from the group</param>
        public async Task RemoveMember(string groupId, string email)
        {
            string uri = String.Format("{0}/members?email={1}", HttpUtility.UrlEncode(groupId), HttpUtility.UrlEncode(email));
            await Api.Delete(uri);
        }
        #endregion

        #region Group Feed/Posts
        /// <summary>
        /// Gets all the posts for the provided group.
        /// </summary>
        /// <remarks>
        /// I added the uri as a parameter since the API doesn't return the "after cursor"
        /// only a full URI for the next call to the API.
        /// </remarks>
        /// <param name="groupId">The group that we want to get posts from</param>
        /// <param name="uri">The next uri used for paging</param>
        private async Task<FeedResponse> GetPosts(string groupId, string uri = "")
        {
            if (String.IsNullOrEmpty(uri))
            {
                uri = String.Format("{0}/feed?fields=id,message,to,from&limit={1}", HttpUtility.UrlEncode(groupId), DefaultLimit);
            }
            return await Api.Get<FeedResponse>(uri);
        }

        /// <summary>
        /// Get all the posts for the provided group.
        /// </summary>
        public async Task<List<Feed>> GetPosts(string groupId)
        {
            List<Feed> result = new List<Feed>();
            await GetPosts(groupId, result);
            return result;
        }

        /// <summary>
        /// Recursively populate the provided list with all the posts in the group.
        /// </summary>
        private async Task GetPosts(string groupId, List<Feed> result, string uri = "")
        {
            FeedResponse response = await GetPosts(groupId, uri);
            if (response != null && response.data.Any())
            {
                result.AddRange(response.data);

                if (!String.IsNullOrEmpty(response.paging.next))
                {
                    await GetPosts(groupId, result, response.paging.next);
                }
            }
        }
        #endregion
    }
}