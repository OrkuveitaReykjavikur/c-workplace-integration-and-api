﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class ProfileResponse
    {
        public List<Profile> data { get; set; }
    }
}