﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WorkplaceAPI.Models
{
    public class UsersResponse
    {
        public List<string> schemas { get; set; }
        public int totalResults { get; set; }
        public int itemsPerPage { get; set; }
        public int startIndex { get; set; }

        [JsonProperty(PropertyName = "Resources")]
        public List<User> users { get; set; }
    }
}