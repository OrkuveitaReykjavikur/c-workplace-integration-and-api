﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class CommunityGroupsResponse
    {
        public List<Group> data { get; set; }
        public Paging paging { get; set; }
    }
}
