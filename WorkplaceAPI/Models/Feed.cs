﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WorkplaceAPI.Models
{
    public class Feed
    {
        public string id { get; set; }
        public Profile from { get; set; }
        public ProfileResponse to { get; set; }
        public string message { get; set; }

        [JsonProperty(PropertyName = "updated_time")]
        public DateTime updatedTime { get; set; }

        [JsonProperty(PropertyName = "created_time")]
        public DateTime createdTime { get; set; }
    }
}