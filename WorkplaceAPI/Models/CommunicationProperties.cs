﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class CommunicationProperties
    {
        public bool primary { get; set; }
        public string type { get; set; }
        public string value { get; set; }
        public PictureData data { get; set; }
    }
}