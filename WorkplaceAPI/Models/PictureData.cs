﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class PictureData
    {
        public string height { get; set; }
        public bool is_silhouette { get; set; }
        public string url { get; set; }
        public string width { get; set; }

    }
}
