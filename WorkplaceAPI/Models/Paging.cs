﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class Paging
    {
        public Cursor cursors { get; set; }
        public string previous { get; set; }
        public string next { get; set; }
    }
}
