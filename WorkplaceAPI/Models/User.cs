﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WorkplaceAPI.Models
{
    public class User
    {
        public User()
        {
            this.schemas = new List<string>();
            this.photos = new List<CommunicationProperties>();
            this.emails = new List<CommunicationProperties>();
            this.phoneNumbers = new List<CommunicationProperties>();
            this.ims = new List<string>();
            this.roles = new List<string>();
            this.entitlements = new List<string>();
            this.addresses = new List<Address>();
        }

        public List<string> schemas { get; set; }
        public string userName { get; set; }
        public Name name { get; set; }
        public string displayName { get; set; }
        public string title { get; set; }
        public string nickName { get; set; }
        public string profileUrl { get; set; }
        public string preferredLanguage { get; set; }
        public string locale { get; set; }
        public string timezone { get; set; }
        public bool active { get; set; }
        public string userType { get; set; }
        public List<CommunicationProperties> photos { get; set; } // Might be a list of CommunicationProperties, then I might want to rename
        public List<CommunicationProperties> emails { get; set; }
        public List<CommunicationProperties> phoneNumbers { get; set; }
        public List<string> ims { get; set; }
        public List<string> roles { get; set; }
        public List<string> entitlements { get; set; }
        public List<Address> addresses { get; set; }
        public string id { get; set; }
        public Guid externalId { get; set; }

        [JsonProperty(PropertyName = "urn:scim:schemas:extension:enterprise:1.0")]
        public Enterprise enterprise { get; set; }
    }
}