﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class Enterprise
    {
        public string employeeNumber { get; set; }
        public string costCenter { get; set; }
        public string organization { get; set; }
        public string division { get; set; }
        public string department { get; set; }
        public Manager manager { get; set; }
    }
}