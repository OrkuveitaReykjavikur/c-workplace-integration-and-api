﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class ProfilePicture
    {
        public byte[] Content { get; set; }
        public string Filename { get; set; }
        public string Type { get; set; }

    }
}
