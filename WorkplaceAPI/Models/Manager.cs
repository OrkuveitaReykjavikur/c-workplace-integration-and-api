﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class Manager
    {
        public string managerId { get; set; }
        public string displayName { get; set; }
    }
}