﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class Cursor
    {
        public string after { get; set; }
        public string before { get; set; }
    }
}