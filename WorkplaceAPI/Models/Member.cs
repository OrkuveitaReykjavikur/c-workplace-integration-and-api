﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace WorkplaceAPI.Models
{
    public class Member
    {
        public string id { get; set; }
        public string email { get; set; }
        public CommunicationProperties picture { get; set; }
        public Uri link { get; set; }
        public string locale { get; set; }
        public string name { get; set; }
        public bool administrator { get; set; }

        [JsonProperty(PropertyName = "first_name")]
        public string firstName { get; set; }

        [JsonProperty(PropertyName = "last_name")]
        public string lastName { get; set; }

        [JsonProperty(PropertyName = "name_format")]
        public string nameFormat { get; set; }

        [JsonProperty(PropertyName = "employee_number")]
        public string employeeNumber { get; set; }
    }
}