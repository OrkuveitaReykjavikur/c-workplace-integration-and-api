﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorkplaceAPI.Models
{
    public class Group
    {
        public string id { get; set; }
        public string description { get; set; }
        public string email { get; set; }
        public string name { get; set; }
        public string privacy { get; set; }
    }
}