# README #



### Update 17/12/21, might be some breaking changes ###

#### Update 

#####  Updated the Project to handle changes made on Workolace Side to updated Endpoints, we are now using SCIM 2.0 compatible endpoints.  #####
- The previous version is broken due to API changes on Workplace.
#####  Some Libraries were updated. #####
##### .NET Version changed. #####

##### Photo Sync #####
###### Support added for customizing the "The field under the phonenumber property that has the IDs for the users for the ProfileImages" was Hardcodes with "pager" in the previous version, "fax" now default.  ######
######  Update Picture functionality now supports uploading photos from locations that are not Internet Accessible, System.IO.File.ReadAllByte () ######
> Only tested on Files located on Windows Network Share. 
> Only available for Photo Syncing all Users, individual user sync is not supported for uploading from local sources. 
> JPG and PNG Supported, JPG default  
> DryRun functionality added for Photo Sync all Users.  

#### Workplace Permissions
Permissions:
Read group membership,Provision user accounts,Manage accounts,Manage groups,Manage work profiles,Read group content and Read work profile

These are privileges permissions and we do suggest locking down the API Key to a Whitelist of IPs. 
The default behaviour of Workplace unless disabled is to remove unused permissions. 
You could run run into the scenario that if you are only using this Sync for either Photos of Groups that you loose permissions for running the other kind of Sync, and a Workplace Administrator would need manually added them again if you would also like to run the other kind of sync. 

![Workplace Permissions](Workplace Permissions.PNG)

#### Other Info 
If you encounter problems in running the program it could be good to check Workplace Permissions or Rate Limiting. 

Documentation here below has not been updated to reflect the changes mentioned here. 

### What is this repository for? ###

This project was created beacuse we needed a tool to sync the Workplace groups with our ActiveDirectory groups
and to set the default profile photos of all Workplace members.

We implemented a simple HTTP client on top of the two Facebook APIs,
i.e. the Graph API to interact with our Workplace groups/members
and the AccountManagement API in order to update users (add default profile photos).

We also implemented a client that could return members from ActiveDirectory.

Finally we decided to define our relationships between ActiveDirectory and Workplace in a group on Workplace.

This "Sync Configuration" group had multiple posts of the following format:
>	AD = {AD_NAME}

>	Workplace = {WORKPLACE_GROUP_ID}

>	Extra Members:

>	@{EXTRA_MEMBER_1}

>	@{EXTRA_MEMBER_2}

All the posts from that group were then parsed to determine if we had to
add or remove members from Workplace group.

Version 1.0.0

### How do I get set up? ###

Update the app.config to use the correct values for your company and run the application.

We synced our profile photos using the "pager" field in the profiles, as we had on a internet facing side all profiles pictures on a url like: https://wpphotos.example.com/[pagernumber].jpg -- your milage might vary here.

We run the CMD executer in the task Scheduler (one schedule for the profile photos and one schedule for the group sync) on the same server we use AD Sync Tool from Facebook.

### What IF we can improve the API/tools ###

Please feel free to issue a pull requests to us. We don't want to break existing use cases, so if you make changes please extend the existing use cases or add new functions/API calls.

### License ###

    Copyright (C) 2017  Orkuveita Reykjavíkur

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

### Who do I talk to? ###

Anton Sigurjónsson (antons[at]or.is)