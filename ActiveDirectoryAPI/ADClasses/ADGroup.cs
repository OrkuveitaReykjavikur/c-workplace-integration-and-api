﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectoryAPI.ADClasses
{
    
    public class ADGroup
    {

        public string Displayname { get; set; }

        public string DistinguishedName { get; set; }

        public string SamAccountName { get; set; }
    }
}
