﻿using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;

namespace ActiveDirectoryAPI.ADClasses
{
    [DirectoryRdnPrefix("CN")]
    [DirectoryObjectClass("Person")]
    class UserPrincipalExtended : UserPrincipal
    {

        // Inplement the constructor using the base class constructor. 
        public UserPrincipalExtended(PrincipalContext context)
            : base(context)
        { }

        // Implement the constructor with initialization parameters.    
        public UserPrincipalExtended(PrincipalContext context,
                             string samAccountName,
                             string password,
                             bool enabled)
            : base(context, samAccountName, password, enabled)
        { }

        // Create the "Manager" property.    
        [DirectoryProperty("manager")]
        public string Manager
        {
            get
            {
                if (ExtensionGet("manager").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("manager")[0];
            }
            set { ExtensionSet("manager", value); }
        }

        //physicalDeliveryOfficeName
        [DirectoryProperty("physicalDeliveryOfficeName")]
        public string physicalDeliveryOfficeName
        {
            get
            {
                if (ExtensionGet("physicalDeliveryOfficeName").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("physicalDeliveryOfficeName")[0];
            }
            set { ExtensionSet("physicalDeliveryOfficeName", value); }
        }

        [DirectoryProperty("pager")]
        public string Pager
        {
            get
            {
                if (ExtensionGet("pager").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("pager")[0];
            }
            set { ExtensionSet("pager", value); }
        }

        [DirectoryProperty("mobile")]
        public string Mobile
        {
            get
            {
                if (ExtensionGet("mobile").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("mobile")[0];
            }
            set { ExtensionSet("mobile", value); }
        }

        [DirectoryProperty("primaryGroupID")]
        public string PrimaryGroupID
        {
            get
            {
                if (ExtensionGet("primaryGroupID").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("primaryGroupID")[0];
            }
            set { ExtensionSet("primaryGroupID", value); }
        }


        [DirectoryProperty("homePhone")]
        public string HomePhone
        {
            get
            {
                if (ExtensionGet("homePhone").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("homePhone")[0];
            }
            set { ExtensionSet("homePhone", value); }
        }


        [DirectoryProperty("company")]
        public string Company
        {
            get
            {
                if (ExtensionGet("company").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("company")[0];
            }
            set { ExtensionSet("company", value); }
        }

        [DirectoryProperty("department")]
        public string department
        {
            get
            {
                if (ExtensionGet("department").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("department")[0];
            }
            set { ExtensionSet("department", value); }
        }

        [DirectoryProperty("division")]
        public string division
        {
            get
            {
                if (ExtensionGet("division").Length != 1)
                    return string.Empty;

                return (string)ExtensionGet("division")[0];
            }
            set { ExtensionSet("division", value); }
        }

        // Implement the overloaded search method FindByIdentity.
        public static new UserPrincipalExtended FindByIdentity(PrincipalContext context, string identityValue)
        {
            return (UserPrincipalExtended)FindByIdentityWithType(context, typeof(UserPrincipalExtended), identityValue);
        }

        // Implement the overloaded search method FindByIdentity. 
        public static new UserPrincipalExtended FindByIdentity(PrincipalContext context, IdentityType identityType, string identityValue)
        {
            return (UserPrincipalExtended)FindByIdentityWithType(context, typeof(UserPrincipalExtended), identityType, identityValue);
        }

    }
}
