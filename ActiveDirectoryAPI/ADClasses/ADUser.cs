﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectoryAPI.ADClasses
{
    public class ADUser
    {
        public string Displayname { get; set; }

        public string SAMAccountName { get; set; }

        public string emailAddress { get; set; }

    }
}
