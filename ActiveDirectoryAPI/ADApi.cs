﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.DirectoryServices.AccountManagement;
using ActiveDirectoryAPI.ADClasses;

namespace ActiveDirectoryAPI
{
    public class ADApi
    {
        public static List<ADUser> getUsersMemberOfGroup(string domainNameOfGroup, string domainName, string domainUrl, ref int resultCode, ref string resultText)
        {
            List<ADUser> allMembers = new List<ADUser>();

            resultCode = 1;
            resultText = "Unknown Error";

            PrincipalContext principalContext = null;
            try
            {
                principalContext = new PrincipalContext(ContextType.Domain, domainName, domainUrl);
            }
            catch (Exception e)
            {
                resultCode = 2;
                resultText = "Error creating principal context " + e.StackTrace;
            }


            if (principalContext != null)
            {
                GroupPrincipal group = null;
                try
                {
                    group = GroupPrincipal.FindByIdentity(principalContext, domainNameOfGroup);
                }
                catch (Exception e)
                {
                    resultCode = 3;
                    resultText = "Error fetching domain group " + e.StackTrace;
                }

                if (group != null)
                {
                    if (group.Members.Count > 0)
                    {
                        try
                        {
                            PrincipalSearchResult<Principal> memberResults = group.GetMembers(true);
                            foreach (UserPrincipal usr in memberResults)
                            {
                                if (usr == null)
                                {
                                    return null;
                                }
                                else
                                {
                                    ADUser user = new ADUser();

                                    user.SAMAccountName = usr.SamAccountName;
                                    user.emailAddress = usr.EmailAddress;
                                    user.Displayname = usr.DisplayName;
                                    allMembers.Add(user);
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            resultCode = 4;
                            resultText = "Error fetching user information " + e.StackTrace;
                            return null;
                        }
                    }
                    else
                    {
                        resultCode = 4;
                        resultText = "No users found in AD group: " + domainNameOfGroup;
                    }
                }
            }

            resultCode = 0;
            return allMembers;
        }
    }
}
